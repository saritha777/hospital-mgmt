'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">mockathon documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-AppModule-4a042e2a3f2d1a2bfd2ed264a3e25213c8a50eaf008ed5894e6076bad0502c21e2cbbf5ab6e0bff92349751b1860a8364a57da3b62d64a795c60302f66362bbb"' : 'data-target="#xs-controllers-links-module-AppModule-4a042e2a3f2d1a2bfd2ed264a3e25213c8a50eaf008ed5894e6076bad0502c21e2cbbf5ab6e0bff92349751b1860a8364a57da3b62d64a795c60302f66362bbb"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AppModule-4a042e2a3f2d1a2bfd2ed264a3e25213c8a50eaf008ed5894e6076bad0502c21e2cbbf5ab6e0bff92349751b1860a8364a57da3b62d64a795c60302f66362bbb"' :
                                            'id="xs-controllers-links-module-AppModule-4a042e2a3f2d1a2bfd2ed264a3e25213c8a50eaf008ed5894e6076bad0502c21e2cbbf5ab6e0bff92349751b1860a8364a57da3b62d64a795c60302f66362bbb"' }>
                                            <li class="link">
                                                <a href="controllers/AppController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AppModule-4a042e2a3f2d1a2bfd2ed264a3e25213c8a50eaf008ed5894e6076bad0502c21e2cbbf5ab6e0bff92349751b1860a8364a57da3b62d64a795c60302f66362bbb"' : 'data-target="#xs-injectables-links-module-AppModule-4a042e2a3f2d1a2bfd2ed264a3e25213c8a50eaf008ed5894e6076bad0502c21e2cbbf5ab6e0bff92349751b1860a8364a57da3b62d64a795c60302f66362bbb"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AppModule-4a042e2a3f2d1a2bfd2ed264a3e25213c8a50eaf008ed5894e6076bad0502c21e2cbbf5ab6e0bff92349751b1860a8364a57da3b62d64a795c60302f66362bbb"' :
                                        'id="xs-injectables-links-module-AppModule-4a042e2a3f2d1a2bfd2ed264a3e25213c8a50eaf008ed5894e6076bad0502c21e2cbbf5ab6e0bff92349751b1860a8364a57da3b62d64a795c60302f66362bbb"' }>
                                        <li class="link">
                                            <a href="injectables/AppService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/BookOperationModule.html" data-type="entity-link" >BookOperationModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-BookOperationModule-cd512b508dbc3d60b295b1eaa1365e11d7a47ca3e37408c7e6a3b43ec52e518b334cd52fef8df88ef9b7130a3c7a9d1fdfea45be8b1f38222e1b607a9b1f76ea"' : 'data-target="#xs-controllers-links-module-BookOperationModule-cd512b508dbc3d60b295b1eaa1365e11d7a47ca3e37408c7e6a3b43ec52e518b334cd52fef8df88ef9b7130a3c7a9d1fdfea45be8b1f38222e1b607a9b1f76ea"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-BookOperationModule-cd512b508dbc3d60b295b1eaa1365e11d7a47ca3e37408c7e6a3b43ec52e518b334cd52fef8df88ef9b7130a3c7a9d1fdfea45be8b1f38222e1b607a9b1f76ea"' :
                                            'id="xs-controllers-links-module-BookOperationModule-cd512b508dbc3d60b295b1eaa1365e11d7a47ca3e37408c7e6a3b43ec52e518b334cd52fef8df88ef9b7130a3c7a9d1fdfea45be8b1f38222e1b607a9b1f76ea"' }>
                                            <li class="link">
                                                <a href="controllers/BookOperationController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >BookOperationController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-BookOperationModule-cd512b508dbc3d60b295b1eaa1365e11d7a47ca3e37408c7e6a3b43ec52e518b334cd52fef8df88ef9b7130a3c7a9d1fdfea45be8b1f38222e1b607a9b1f76ea"' : 'data-target="#xs-injectables-links-module-BookOperationModule-cd512b508dbc3d60b295b1eaa1365e11d7a47ca3e37408c7e6a3b43ec52e518b334cd52fef8df88ef9b7130a3c7a9d1fdfea45be8b1f38222e1b607a9b1f76ea"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-BookOperationModule-cd512b508dbc3d60b295b1eaa1365e11d7a47ca3e37408c7e6a3b43ec52e518b334cd52fef8df88ef9b7130a3c7a9d1fdfea45be8b1f38222e1b607a9b1f76ea"' :
                                        'id="xs-injectables-links-module-BookOperationModule-cd512b508dbc3d60b295b1eaa1365e11d7a47ca3e37408c7e6a3b43ec52e518b334cd52fef8df88ef9b7130a3c7a9d1fdfea45be8b1f38222e1b607a9b1f76ea"' }>
                                        <li class="link">
                                            <a href="injectables/BookOperationService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >BookOperationService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/ConfigurationModule.html" data-type="entity-link" >ConfigurationModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-ConfigurationModule-03b1bf50177e65c603ab9f20f66438f5f16291b766a586971b8224eefe882b5582d9aae0258389286e74a494b8dc97bb0b9d79c78080d3707e7826295683e3fd"' : 'data-target="#xs-controllers-links-module-ConfigurationModule-03b1bf50177e65c603ab9f20f66438f5f16291b766a586971b8224eefe882b5582d9aae0258389286e74a494b8dc97bb0b9d79c78080d3707e7826295683e3fd"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-ConfigurationModule-03b1bf50177e65c603ab9f20f66438f5f16291b766a586971b8224eefe882b5582d9aae0258389286e74a494b8dc97bb0b9d79c78080d3707e7826295683e3fd"' :
                                            'id="xs-controllers-links-module-ConfigurationModule-03b1bf50177e65c603ab9f20f66438f5f16291b766a586971b8224eefe882b5582d9aae0258389286e74a494b8dc97bb0b9d79c78080d3707e7826295683e3fd"' }>
                                            <li class="link">
                                                <a href="controllers/ConfigurationController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ConfigurationController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-ConfigurationModule-03b1bf50177e65c603ab9f20f66438f5f16291b766a586971b8224eefe882b5582d9aae0258389286e74a494b8dc97bb0b9d79c78080d3707e7826295683e3fd"' : 'data-target="#xs-injectables-links-module-ConfigurationModule-03b1bf50177e65c603ab9f20f66438f5f16291b766a586971b8224eefe882b5582d9aae0258389286e74a494b8dc97bb0b9d79c78080d3707e7826295683e3fd"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-ConfigurationModule-03b1bf50177e65c603ab9f20f66438f5f16291b766a586971b8224eefe882b5582d9aae0258389286e74a494b8dc97bb0b9d79c78080d3707e7826295683e3fd"' :
                                        'id="xs-injectables-links-module-ConfigurationModule-03b1bf50177e65c603ab9f20f66438f5f16291b766a586971b8224eefe882b5582d9aae0258389286e74a494b8dc97bb0b9d79c78080d3707e7826295683e3fd"' }>
                                        <li class="link">
                                            <a href="injectables/ConfigurationService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ConfigurationService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/StaffModule.html" data-type="entity-link" >StaffModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-StaffModule-2783598253530b819d0bac1f0ece8d1a92ac02b5c4d7b3b7a43e0a88f1e6896f7e1d70f107881030e47e1913165fbcde5c186dddfe5397daf4b27caac3c4ed3d"' : 'data-target="#xs-controllers-links-module-StaffModule-2783598253530b819d0bac1f0ece8d1a92ac02b5c4d7b3b7a43e0a88f1e6896f7e1d70f107881030e47e1913165fbcde5c186dddfe5397daf4b27caac3c4ed3d"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-StaffModule-2783598253530b819d0bac1f0ece8d1a92ac02b5c4d7b3b7a43e0a88f1e6896f7e1d70f107881030e47e1913165fbcde5c186dddfe5397daf4b27caac3c4ed3d"' :
                                            'id="xs-controllers-links-module-StaffModule-2783598253530b819d0bac1f0ece8d1a92ac02b5c4d7b3b7a43e0a88f1e6896f7e1d70f107881030e47e1913165fbcde5c186dddfe5397daf4b27caac3c4ed3d"' }>
                                            <li class="link">
                                                <a href="controllers/StaffController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >StaffController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-StaffModule-2783598253530b819d0bac1f0ece8d1a92ac02b5c4d7b3b7a43e0a88f1e6896f7e1d70f107881030e47e1913165fbcde5c186dddfe5397daf4b27caac3c4ed3d"' : 'data-target="#xs-injectables-links-module-StaffModule-2783598253530b819d0bac1f0ece8d1a92ac02b5c4d7b3b7a43e0a88f1e6896f7e1d70f107881030e47e1913165fbcde5c186dddfe5397daf4b27caac3c4ed3d"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-StaffModule-2783598253530b819d0bac1f0ece8d1a92ac02b5c4d7b3b7a43e0a88f1e6896f7e1d70f107881030e47e1913165fbcde5c186dddfe5397daf4b27caac3c4ed3d"' :
                                        'id="xs-injectables-links-module-StaffModule-2783598253530b819d0bac1f0ece8d1a92ac02b5c4d7b3b7a43e0a88f1e6896f7e1d70f107881030e47e1913165fbcde5c186dddfe5397daf4b27caac3c4ed3d"' }>
                                        <li class="link">
                                            <a href="injectables/StaffService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >StaffService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/UserModule.html" data-type="entity-link" >UserModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-UserModule-f2809c0ae99c7c1733662fd07fdc9a7364f1c7cf862da8acb6d6149adc5c2c5296d9c77a12001d11ea3183c7ff5c47feb7156bbeb8c9b0b423f54cad9dbdf968"' : 'data-target="#xs-controllers-links-module-UserModule-f2809c0ae99c7c1733662fd07fdc9a7364f1c7cf862da8acb6d6149adc5c2c5296d9c77a12001d11ea3183c7ff5c47feb7156bbeb8c9b0b423f54cad9dbdf968"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-UserModule-f2809c0ae99c7c1733662fd07fdc9a7364f1c7cf862da8acb6d6149adc5c2c5296d9c77a12001d11ea3183c7ff5c47feb7156bbeb8c9b0b423f54cad9dbdf968"' :
                                            'id="xs-controllers-links-module-UserModule-f2809c0ae99c7c1733662fd07fdc9a7364f1c7cf862da8acb6d6149adc5c2c5296d9c77a12001d11ea3183c7ff5c47feb7156bbeb8c9b0b423f54cad9dbdf968"' }>
                                            <li class="link">
                                                <a href="controllers/UserController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UserController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-UserModule-f2809c0ae99c7c1733662fd07fdc9a7364f1c7cf862da8acb6d6149adc5c2c5296d9c77a12001d11ea3183c7ff5c47feb7156bbeb8c9b0b423f54cad9dbdf968"' : 'data-target="#xs-injectables-links-module-UserModule-f2809c0ae99c7c1733662fd07fdc9a7364f1c7cf862da8acb6d6149adc5c2c5296d9c77a12001d11ea3183c7ff5c47feb7156bbeb8c9b0b423f54cad9dbdf968"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-UserModule-f2809c0ae99c7c1733662fd07fdc9a7364f1c7cf862da8acb6d6149adc5c2c5296d9c77a12001d11ea3183c7ff5c47feb7156bbeb8c9b0b423f54cad9dbdf968"' :
                                        'id="xs-injectables-links-module-UserModule-f2809c0ae99c7c1733662fd07fdc9a7364f1c7cf862da8acb6d6149adc5c2c5296d9c77a12001d11ea3183c7ff5c47feb7156bbeb8c9b0b423f54cad9dbdf968"' }>
                                        <li class="link">
                                            <a href="injectables/JwtToken.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >JwtToken</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UserService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UserService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#controllers-links"' :
                                'data-target="#xs-controllers-links"' }>
                                <span class="icon ion-md-swap"></span>
                                <span>Controllers</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="controllers-links"' : 'id="xs-controllers-links"' }>
                                <li class="link">
                                    <a href="controllers/AppController.html" data-type="entity-link" >AppController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/BookOperationController.html" data-type="entity-link" >BookOperationController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/ConfigurationController.html" data-type="entity-link" >ConfigurationController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/StaffController.html" data-type="entity-link" >StaffController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/UserController.html" data-type="entity-link" >UserController</a>
                                </li>
                            </ul>
                        </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#entities-links"' :
                                'data-target="#xs-entities-links"' }>
                                <span class="icon ion-ios-apps"></span>
                                <span>Entities</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="entities-links"' : 'id="xs-entities-links"' }>
                                <li class="link">
                                    <a href="entities/OperationBooking.html" data-type="entity-link" >OperationBooking</a>
                                </li>
                                <li class="link">
                                    <a href="entities/OperationCategory.html" data-type="entity-link" >OperationCategory</a>
                                </li>
                                <li class="link">
                                    <a href="entities/OperationTheaterCategory.html" data-type="entity-link" >OperationTheaterCategory</a>
                                </li>
                                <li class="link">
                                    <a href="entities/Staff.html" data-type="entity-link" >Staff</a>
                                </li>
                                <li class="link">
                                    <a href="entities/StaffCategory.html" data-type="entity-link" >StaffCategory</a>
                                </li>
                                <li class="link">
                                    <a href="entities/User.html" data-type="entity-link" >User</a>
                                </li>
                                <li class="link">
                                    <a href="entities/UserLogin.html" data-type="entity-link" >UserLogin</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/AuthenticationException.html" data-type="entity-link" >AuthenticationException</a>
                            </li>
                            <li class="link">
                                <a href="classes/DataNotFoundException.html" data-type="entity-link" >DataNotFoundException</a>
                            </li>
                            <li class="link">
                                <a href="classes/globalAccess.html" data-type="entity-link" >globalAccess</a>
                            </li>
                            <li class="link">
                                <a href="classes/HttpExceptionFilter.html" data-type="entity-link" >HttpExceptionFilter</a>
                            </li>
                            <li class="link">
                                <a href="classes/LoginDto.html" data-type="entity-link" >LoginDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/Maintainance.html" data-type="entity-link" >Maintainance</a>
                            </li>
                            <li class="link">
                                <a href="classes/operationBookingDto.html" data-type="entity-link" >operationBookingDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/OperationCategoryDto.html" data-type="entity-link" >OperationCategoryDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/OpertaionTheaterCategoryDto.html" data-type="entity-link" >OpertaionTheaterCategoryDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/StaffCategoryDto.html" data-type="entity-link" >StaffCategoryDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/StaffDto.html" data-type="entity-link" >StaffDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/UnauthorizedException.html" data-type="entity-link" >UnauthorizedException</a>
                            </li>
                            <li class="link">
                                <a href="classes/UserDto.html" data-type="entity-link" >UserDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/UserNotFoundException.html" data-type="entity-link" >UserNotFoundException</a>
                            </li>
                            <li class="link">
                                <a href="classes/ValidationExceptionFilter.html" data-type="entity-link" >ValidationExceptionFilter</a>
                            </li>
                            <li class="link">
                                <a href="classes/WrongCredentialsException.html" data-type="entity-link" >WrongCredentialsException</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AppService.html" data-type="entity-link" >AppService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/BookOperationService.html" data-type="entity-link" >BookOperationService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CacheManagerInterceptor.html" data-type="entity-link" >CacheManagerInterceptor</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ConfigurationService.html" data-type="entity-link" >ConfigurationService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/JwtToken.html" data-type="entity-link" >JwtToken</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/Logger.html" data-type="entity-link" >Logger</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LoggerMiddleware.html" data-type="entity-link" >LoggerMiddleware</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/StaffService.html" data-type="entity-link" >StaffService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UserService.html" data-type="entity-link" >UserService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ValidationPipes.html" data-type="entity-link" >ValidationPipes</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/RolesGuard.html" data-type="entity-link" >RolesGuard</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/SwaggerConfig.html" data-type="entity-link" >SwaggerConfig</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});