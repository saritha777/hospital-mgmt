import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { LoggerMiddleware } from './common/middleware/logger.middleware';
import { BookOperationModule } from './modules/bookOperation/bookOperation.module';
import { ConfigurationModule } from './modules/configurationTables/configuration.module';
import { StaffModule } from './modules/staff/staff.module';
import { UserModule } from './modules/user/user.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: 'root',
      database: 'hospital',
      autoLoadEntities: true,
      synchronize: true,
    }),
    UserModule,
    ConfigurationModule,
    StaffModule,
    BookOperationModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(LoggerMiddleware).forRoutes('*');
  }
}
