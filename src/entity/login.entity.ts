import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Staff } from './staff.entity';
import { User } from './user.entity';

/**
 * user login entity
 */
@Entity()
export class UserLogin {
  /**
   * primary generated column for the id
   */
  @PrimaryGeneratedColumn()
  id: number;

  /**
   * aadhar no property
   */
  @ApiProperty()
  @IsString()
  @Column()
  email: string;

  /**
   * password property
   */
  @ApiProperty()
  @IsString()
  @Column()
  password: string;

  @Column()
  role: string;

  @OneToOne(() => User, (user) => user.login, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn()
  user: User;
}
