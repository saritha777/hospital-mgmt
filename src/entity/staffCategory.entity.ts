import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import {
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Staff } from './staff.entity';

/**
 * staff category entity
 */
@Entity()
export class StaffCategory {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column()
  @IsString()
  staffCategoryname: string;

  @OneToMany(() => Staff, (staff) => staff.staffCategory)
  staff: Staff[];

  addStaff(staff: Staff) {
    if (this.staff == null) {
      this.staff = new Array<Staff>();
    }

    return this.staff.push(staff);
  }
}
