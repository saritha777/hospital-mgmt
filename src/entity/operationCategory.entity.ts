import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { OperationBooking } from './operationBooking.entity';

/**
 * operation catetory entity
 */
@Entity()
export class OperationCategory {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @IsString()
  @Column()
  operationCategoryName: string;

  @OneToMany(
    () => OperationBooking,
    (operationBooking) => operationBooking.opertaionCategory,
  )
  operationBooking: OperationBooking[];
}
