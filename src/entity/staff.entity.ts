import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsInt,
  IsString,
  Matches,
  MaxLength,
  MinLength,
} from 'class-validator';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Maintainance } from './maintainance';
import { OperationBooking } from './operationBooking.entity';
import { StaffCategory } from './staffCategory.entity';

/**
 * user entity
 */
@Entity()
export class Staff extends Maintainance {
  /**
   * id as primary generated column
   */
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @IsString()
  @Column()
  firstName: string;

  /**
   * last name property of staff
   */
  @ApiProperty()
  @IsString()
  @Column()
  lastName: string;

  @ApiProperty()
  @IsString()
  @Column()
  email: string;

  @ApiProperty()
  @IsInt()
  @Column()
  staffCategoryId: number;

  @ApiProperty()
  @IsString()
  @Column()
  DOB: string;

  @ApiProperty()
  @IsString()
  @Column()
  phoneNo: string;

  @ApiProperty()
  @IsString()
  @Column()
  addressLine1: string;

  @ApiProperty()
  @IsString()
  @Column()
  addressLine2: string;

  @ApiProperty()
  @IsString()
  @Column()
  city: string;

  @ApiProperty()
  @IsString()
  @Column()
  state: string;

  @ManyToOne(() => StaffCategory, (staffCategory) => staffCategory.staff, {
    cascade: true,
  })
  @JoinColumn()
  staffCategory: StaffCategory;

  @OneToMany(
    () => OperationBooking,
    (operationBooking) => operationBooking.staff,
  )
  operationBooking: OperationBooking[];
}
