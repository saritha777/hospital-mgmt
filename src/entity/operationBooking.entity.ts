import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString } from 'class-validator';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { OperationCategory } from './operationCategory.entity';
import { OperationTheaterCategory } from './operationTheaterCategory.entity';
import { Staff } from './staff.entity';

/**
 * operation booking entity
 *
 */
@Entity()
export class OperationBooking {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @IsString()
  @Column()
  doctorName: string;

  @ApiProperty()
  @IsString()
  @Column()
  fromDate: string;

  @ApiProperty()
  @IsString()
  @Column()
  toDate: string;

  @ApiProperty()
  @IsInt()
  @Column()
  staffId: number;

  @ApiProperty()
  @IsString()
  @Column()
  operationTheaterCategoryId: number;

  @ApiProperty()
  @IsString()
  @Column()
  operationCategoryId: number;

  @Column()
  operationCategoryName: string;
  @ManyToOne(() => Staff, (staff) => staff.operationBooking, {
    cascade: true,
  })
  @JoinColumn()
  staff: Staff;

  @ManyToOne(
    () => OperationTheaterCategory,
    (opertaionTheaterCategory) => opertaionTheaterCategory.operationBooking,
    {
      cascade: true,
    },
  )
  @JoinColumn()
  opertaionTheaterCategory: OperationTheaterCategory;

  @ManyToOne(
    () => OperationCategory,
    (opertaionCategory) => opertaionCategory.operationBooking,
    {
      cascade: true,
    },
  )
  @JoinColumn()
  opertaionCategory: OperationCategory;
}
