import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { OperationBooking } from './operationBooking.entity';

/**
 * Operation Theater Category entity
 */
@Entity()
export class OperationTheaterCategory {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @IsString()
  @Column()
  operationTheaterName: string;

  @OneToMany(
    () => OperationBooking,
    (operationBooking) => operationBooking.opertaionTheaterCategory,
  )
  operationBooking: OperationBooking[];
}
