import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UnauthorizedException } from 'src/common/filters/custom-exception';
import { operationBookingDto } from 'src/dto/operationBookin.dto';
import { OperationBooking } from 'src/entity/operationBooking.entity';
import { OperationCategory } from 'src/entity/operationCategory.entity';
import { OperationTheaterCategory } from 'src/entity/operationTheaterCategory.entity';
import { Staff } from 'src/entity/staff.entity';
import { Repository } from 'typeorm';

/**
 * book operation service
 */
@Injectable()
export class BookOperationService {
  /**
   * injecting all repositories
   * @param operationBookingRepository  injecting operationBookingRepository
   * @param operationCategoryRepository injecting operationCategoryRepository
   * @param OperationTheaterCategoryRepository injecting OperationTheaterCategoryRepository
   * @Param
   */
  constructor(
    @InjectRepository(OperationBooking)
    private operationBookingRepository: Repository<OperationBooking>,
    @InjectRepository(OperationCategory)
    private operationCategoryRepository: Repository<OperationCategory>,
    @InjectRepository(OperationTheaterCategory)
    private OperationTheaterCategoryRepository: Repository<OperationTheaterCategory>,
    @InjectRepository(Staff)
    private staffRepository: Repository<Staff>,
  ) {}

  /**
   * operation booking method
   * @param data taking operation booking data
   * @returns savinga data
   */
  async opeartionBooking(data: operationBookingDto): Promise<OperationBooking> {
    const operationBookingData = new OperationBooking();
    const operationCategoryData =
      await this.operationCategoryRepository.findOne({
        id: operationBookingData.operationCategoryId,
      });
    if (!operationCategoryData) {
      throw new UnauthorizedException();
    }
    const operationTheaterCategoryData =
      await this.OperationTheaterCategoryRepository.findOne({
        id: operationBookingData.operationTheaterCategoryId,
      });
    const staffData = await this.staffRepository.findOne({
      id: operationBookingData.staffId,
    });
    operationBookingData.staff = staffData;
    operationBookingData.opertaionCategory = operationCategoryData;
    operationBookingData.opertaionTheaterCategory =
      operationTheaterCategoryData;
    const newData = Object.assign(operationBookingData, data);
    return this.operationBookingRepository.save(newData);
  }

  /**
   * get all info
   * @returns all booked info
   */
  async getAllOperationBookingInfo(): Promise<OperationBooking[]> {
    return this.operationBookingRepository.find();
  }

  /**
   *get booking info based on category name
   * @param operationCategoryName taking operationCategoryName
   * @returns all info based on name
   */
  async getBookingInfoBasedOnCategory(
    operationCategoryName: string,
  ): Promise<OperationBooking[]> {
    return this.operationBookingRepository.find({
      operationCategoryName: operationCategoryName,
    });
  }

  /**
   * update booked info
   * @param id taking id which we need to update
   * @param data pupdated details
   * @returns updated info
   */
  async updateOperationBookingInfo(
    id: number,
    data: operationBookingDto,
  ): Promise<string> {
    const operationBookingData = new OperationBooking();
    const operationCategoryData =
      await this.operationCategoryRepository.findOne({
        id: operationBookingData.operationCategoryId,
      });
    const operationTheaterCategoryData =
      await this.OperationTheaterCategoryRepository.findOne({
        id: operationBookingData.operationTheaterCategoryId,
      });
    operationBookingData.opertaionCategory = operationCategoryData;
    operationBookingData.opertaionTheaterCategory =
      operationTheaterCategoryData;
    const newData = Object.assign(operationBookingData, data);
    await this.operationBookingRepository.update({ id: id }, newData);
    return 'updated successfully';
  }

  /**
   * cancel booking method
   * @param id taking id
   * @param fromDate taking from date
   * @returns based on id and from date delete will be done
   */
  async cancelBooking(id: number, fromDate: string) {
    const data = await this.operationBookingRepository.findOne({
      fromDate: fromDate,
    });
    if (data.fromDate) {
      return this.operationBookingRepository.delete({ id: id });
    }
  }
}
