import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CompoDecorators } from 'src/common/decorators/compodecorator';
import { operationBookingDto } from 'src/dto/operationBookin.dto';
import { OperationBooking } from 'src/entity/operationBooking.entity';
import { BookOperationService } from './bookoperation.service';

/**
 * book operation controller
 */
@ApiTags('OPERATIONBOOKING')
@Controller('/operationBooking')
export class BookOperationController {
  /**
   * injecting book operation service for business logic
   * @param bookOperationService injecting bookoperationservice
   */
  constructor(private readonly bookOperationService: BookOperationService) {}

  /**
   * operation booking method
   * @param data taking operation booking data
   * @returns savinga data
   */
  @Post()
  @CompoDecorators('receptionist')
  async operationBooking(
    @Body() data: operationBookingDto,
  ): Promise<OperationBooking> {
    return this.bookOperationService.opeartionBooking(data);
  }

  /**
   * get all info
   * @returns all booked info
   */
  @Get()
  getAllOperationBookingInfo(): Promise<OperationBooking[]> {
    const result = this.bookOperationService.getAllOperationBookingInfo();
    if (result) {
      return result;
    } else {
      throw new HttpException('no booking details', HttpStatus.NOT_FOUND);
    }
  }

  /**
   *get booking info based on category name
   * @param operationCategoryName taking operationCategoryName
   * @returns all info based on name
   */
  @Get('/:operationCategoryName')
  getBookingInfoBasedOnCategory(
    @Param('operationCategoryName') operationCategoryName: string,
  ): Promise<OperationBooking[]> {
    const result = this.bookOperationService.getBookingInfoBasedOnCategory(
      operationCategoryName,
    );
    if (result) {
      return result;
    } else {
      throw new HttpException('info not available', HttpStatus.NOT_FOUND);
    }
  }

  /**
   * update booked info
   * @param id taking id which we need to update
   * @param data pupdated details
   * @returns updated info
   */
  @Put('/:id')
  @CompoDecorators('receptionist')
  updateOperationBookingInfo(
    @Param('id') id: number,
    @Body() data: operationBookingDto,
  ): Promise<string> {
    return this.bookOperationService.updateOperationBookingInfo(id, data);
  }

  /**
   * cancel booking method
   * @param id taking id
   * @param fromDate taking from date
   * @returns based on id and from date delete will be done
   */
  @Delete('/:id/:fromDate')
  @CompoDecorators('receptionist')
  cancelBooking(@Param('id') id: number, @Param('fromDate') fromDate: string) {
    return this.bookOperationService.cancelBooking(id, fromDate);
  }
}
