import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OperationBooking } from 'src/entity/operationBooking.entity';
import { OperationCategory } from 'src/entity/operationCategory.entity';
import { OperationTheaterCategory } from 'src/entity/operationTheaterCategory.entity';
import { Staff } from 'src/entity/staff.entity';
import { BookOperationController } from './bookOperation.controller';
import { BookOperationService } from './bookoperation.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      OperationBooking,
      OperationCategory,
      OperationTheaterCategory,
      Staff,
    ]),
  ],
  controllers: [BookOperationController],
  providers: [BookOperationService],
})
export class BookOperationModule {}
