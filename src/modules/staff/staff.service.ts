import {
  HttpCode,
  HttpException,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { StaffDto } from 'src/dto/staff.dto';
import { Staff } from 'src/entity/staff.entity';
import { StaffCategory } from 'src/entity/staffCategory.entity';
import { Repository } from 'typeorm';

/**
 * staff service having all the business logics
 */
@Injectable()
export class StaffService {
  /**
   * injecting all repositories for storing and retriving data
   * @param staffRepository injecting staff repository
   * @param StaffCategoryRepository injecting staff category repository
   */
  constructor(
    @InjectRepository(Staff)
    private staffRepository: Repository<Staff>,
    @InjectRepository(StaffCategory)
    private StaffCategoryRepository: Repository<StaffCategory>,
  ) {}

  /**
   * add method for staff info
   * @param data taking staff data
   * @returns saving staff data
   */
  async addStaffInfo(data: StaffDto) {
    const staffData = new Staff();
    const staffCategoryData = await this.StaffCategoryRepository.findOne({
      id: staffData.staffCategoryId,
    });
    if (!staffCategoryData) {
      throw new HttpException('no data', HttpStatus.BAD_GATEWAY);
    } else {
      staffData.staffCategory = staffCategoryData;
      const newData = Object.assign(staffData, data);
      return this.staffRepository.save(newData);
    }
  }

  /**
   * get method for staff details
   * @returns all staff details
   */
  async getAllStaffInfo() {
    return this.staffRepository.find();
  }

  /**
   * update method for staff
   * @param id taking id which we need to update
   * @param data taking staff data
   * @returns updated info
   */
  async updateStaffInfo(id: number, data: StaffDto) {
    const staffData = new Staff();
    const staffCategoryData = await this.StaffCategoryRepository.findOne({
      id: staffData.staffCategoryId,
    });
    staffData.staffCategory = staffCategoryData;
    const newData = Object.assign(staffData, data);
    return this.staffRepository.update({ id: id }, newData);
  }

  /**
   * remove method
   * @param id taking is which we need to remove
   * @returns deleted info
   */
  removeStaff(id: number) {
    return this.staffRepository.delete({ id: id });
  }
}
