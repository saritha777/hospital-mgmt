import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Staff } from 'src/entity/staff.entity';
import { StaffCategory } from 'src/entity/staffCategory.entity';
import { StaffController } from './staff.controller';
import { StaffService } from './staff.service';

@Module({
  imports: [TypeOrmModule.forFeature([StaffCategory, Staff])],
  controllers: [StaffController],
  providers: [StaffService],
})
export class StaffModule {}
