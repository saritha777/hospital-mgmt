import {
  Controller,
  Post,
  Body,
  Get,
  Put,
  Param,
  Delete,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CompoDecorators } from 'src/common/decorators/compodecorator';
import { StaffDto } from 'src/dto/staff.dto';
import { StaffCategoryDto } from 'src/dto/staffCategory.dto';
import { StaffService } from './staff.service';

/**
 * staff controller
 */
@ApiTags('STAFF')
@Controller('/staff')
export class StaffController {
  /**
   * injecting staff service for using the business logics
   * @param staffService
   */
  constructor(private readonly staffService: StaffService) {}

  /**
   * add method for staff info
   * @param staff taking staff data
   * @returns saving staff info
   */
  @Post()
  @CompoDecorators('admin')
  async addStaffInfo(@Body() staff: StaffDto) {
    return await this.staffService.addStaffInfo(staff);
  }

  /**
   * get method for all staff info
   * @returns all staff info
   */
  @Get()
  async getAllStaffInfo() {
    const result = await this.staffService.getAllStaffInfo();
    if (result) {
      return result;
    } else {
      throw new HttpException('staffInfo not found', HttpStatus.NOT_FOUND);
    }
  }

  /**
   * update method for staff info
   * @param id taking is which we need to update
   * @param staff modified data
   * @returns updated info
   */
  @Put('/:id')
  @CompoDecorators('admin')
  async updateAllStaffInfo(@Param('id') id: number, @Body() staff: StaffDto) {
    return await this.staffService.updateStaffInfo(id, staff);
  }

  /**
   * remove method for staff
   * @param id taking id to remove
   * @returns deted info
   */
  @Delete('/:id')
  @CompoDecorators('admin')
  async removeStaff(@Param('id') id: number) {
    return this.staffService.removeStaff(id);
  }
}
