import { Body, CacheKey, Controller, Get, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { OperationCategoryDto } from 'src/dto/operationCategory.dto';
import { OpertaionTheaterCategoryDto } from 'src/dto/operationTheaterCategory.dto';
import { StaffCategoryDto } from 'src/dto/staffCategory.dto';
import { ConfigurationService } from './configuration.service';

/**
 * configuration controller
 */
@ApiTags('configuration')
@Controller('/configuration')
export class ConfigurationController {
  /**
   * injecting service for all business logics
   * @param ConfigurationService injecting configuration service
   */
  constructor(private readonly ConfigurationService: ConfigurationService) {}

  /**
   * add method for staff category
   * @param staffCategory taking staff data
   * @returns saving data into repository
   */
  @Post('/staff')
  async addStaffCategories(@Body() staffCategory: StaffCategoryDto) {
    return await this.ConfigurationService.addStaffCategories(staffCategory);
  }

  /**
   * add method for operation category
   * @param operationCategory taking operation category data
   * @returns saving data into repository
   */
  @Post('/operationCategory')
  async addOperationCategories(
    @Body() operationCategory: OperationCategoryDto,
  ) {
    return await this.ConfigurationService.addOperationCategories(
      operationCategory,
    );
  }

  /**
   *add method for operationTheaterCategory
   * @param operationTheaterCategory taking operation Theater Category data
   * @returns saving data into repository
   */
  @Post('/operationTheaterCategory')
  async addOperationTheaterCategories(
    @Body() opertaionTheaterCategory: OpertaionTheaterCategoryDto,
  ) {
    return await this.ConfigurationService.addOperationTheaterCategories(
      opertaionTheaterCategory,
    );
  }

  /**
   * get method for staff category
   * @returns all staff category details
   */
  @Get('/staff')
  @CacheKey('staff')
  async getAllStaffCategories() {
    return await this.ConfigurationService.getAllSatffCategories();
  }

  /**
   * get method for operation category
   * @returns all operation category details
   */
  @Get('/operationCategory')
  @CacheKey('operationCategory')
  async getAllOperationCategories() {
    return await this.ConfigurationService.getAllOperationCategories();
  }

  /**
   * get method for Operation Theatre Categories adding caching will delay the operation
   * @returns all Operation Theatre Categories details
   */
  @Get('/operationTheaterCategory')
  @CacheKey('operationTheaterCategory')
  async getAllOperationTheatreCategories() {
    return await this.ConfigurationService.getAllOperationTheatreCategories();
  }
}
