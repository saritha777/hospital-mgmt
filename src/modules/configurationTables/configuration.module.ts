import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OperationBooking } from 'src/entity/operationBooking.entity';
import { OperationCategory } from 'src/entity/operationCategory.entity';
import { OperationTheaterCategory } from 'src/entity/operationTheaterCategory.entity';
import { Staff } from 'src/entity/staff.entity';
import { StaffCategory } from 'src/entity/staffCategory.entity';
import { ConfigurationController } from './configuration.controller';
import { ConfigurationService } from './configuration.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      OperationCategory,
      OperationTheaterCategory,
      StaffCategory,
    ]),
  ],
  controllers: [ConfigurationController],
  providers: [ConfigurationService],
})
export class ConfigurationModule {}
