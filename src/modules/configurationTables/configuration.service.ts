import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { OperationCategoryDto } from 'src/dto/operationCategory.dto';
import { OpertaionTheaterCategoryDto } from 'src/dto/operationTheaterCategory.dto';
import { StaffCategoryDto } from 'src/dto/staffCategory.dto';
import { OperationCategory } from 'src/entity/operationCategory.entity';
import { OperationTheaterCategory } from 'src/entity/operationTheaterCategory.entity';
import { Staff } from 'src/entity/staff.entity';
import { StaffCategory } from 'src/entity/staffCategory.entity';
import { Repository } from 'typeorm';

/**
 * configuration service having all the logics related to StaffCategory,OperationTheaterCategory,OperationTheaterCategory
 */
@Injectable()
export class ConfigurationService {
  /**
   * injecting all the repositories
   * @param staffCategoryRepository injecting StaffCategory
   * @param operationCategoryRepository injecting OperationCategory
   * @param operationTheatreCategoryRepository injecting OperationTheaterCategory
   */
  constructor(
    @InjectRepository(StaffCategory)
    private staffCategoryRepository: Repository<StaffCategory>,
    @InjectRepository(OperationCategory)
    private operationCategoryRepository: Repository<OperationCategory>,

    @InjectRepository(OperationTheaterCategory)
    private operationTheatreCategoryRepository: Repository<OperationTheaterCategory>,
  ) {}

  /**
   * add method for staff category
   * @param staffCategory taking staff data
   * @returns saving data into repository
   */
  async addStaffCategories(staffCategory: StaffCategoryDto) {
    const staffData = new Staff();
    const data = Object.assign(staffData, staffCategory);
    return await this.staffCategoryRepository.save(data);
  }

  /**
   * add method for operation category
   * @param operationCategory taking operation category data
   * @returns saving data into repository
   */
  async addOperationCategories(operationCategory: OperationCategoryDto) {
    const operationCategoryData = new OperationCategory();
    const data = Object.assign(operationCategoryData, operationCategory);
    return await this.operationCategoryRepository.save(data);
  }

  /**
   *add method for operationTheaterCategory
   * @param operationTheaterCategory taking operation Theater Category data
   * @returns saving data into repository
   */
  async addOperationTheaterCategories(
    operationTheaterCategory: OpertaionTheaterCategoryDto,
  ) {
    const operationTheaterCategoryData = new OperationTheaterCategory();
    const data = Object.assign(
      operationTheaterCategoryData,
      operationTheaterCategory,
    );
    return await this.operationTheatreCategoryRepository.save(data);
  }

  /**
   * get method for staff category
   * @returns all staff category details
   */
  getAllSatffCategories() {
    return new Promise((res) => {
      setTimeout(() => {
        res(this.staffCategoryRepository.find());
      }, 3000);
    });
  }

  /**
   * get method for operation category
   * @returns all operation category details
   */
  getAllOperationCategories() {
    return new Promise((res) => {
      setTimeout(() => {
        res(this.operationCategoryRepository.find());
      }, 3000);
    });
  }

  /**
   * get method for Operation Theatre Categories
   * @returns all Operation Theatre Categories details
   */
  getAllOperationTheatreCategories() {
    return new Promise((res) => {
      setTimeout(() => {
        res(this.operationTheatreCategoryRepository.find());
      }, 3000);
    });
  }
}
