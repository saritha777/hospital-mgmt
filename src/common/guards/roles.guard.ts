import {
  CanActivate,
  ExecutionContext,
  HttpException,
  HttpStatus,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';
import { globalAccess } from '../constants/globalAccess';

/**
 * roles guard class implementing form canactivate
 */
@Injectable()
export class RolesGuard implements CanActivate {
  /**
   * injecting reflector for getting data
   * @param reflector injecting reflector
   */
  constructor(private readonly reflector: Reflector) {}
  /**
   * can activate method
   * @param context taking context
   * @returns boolen value having access or hot
   */
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    //  const user = new User();
    const roles = this.reflector.get<string>('roles', context.getHandler());
    console.log(roles);

    if (!roles) {
      return true;
    }

    console.log('role:', globalAccess.role);
    if (roles.includes('admin') && globalAccess.role == 'admin') {
      return true;
    } else if (
      roles.includes('receptionist') &&
      globalAccess.role == 'receptionist'
    ) {
      return true;
    } else if (roles.includes('doctor') && globalAccess.role == 'doctor') {
      return true;
    }
    throw new HttpException(
      'you are not authoried to do this operation',
      HttpStatus.BAD_REQUEST,
    );
  }
}
