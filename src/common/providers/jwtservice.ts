import { Injectable, Res, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserLogin } from '../../entity/login.entity';
import { Response } from 'express';

/**
 * jwt token class
 */
@Injectable()
export class JwtToken {
  /**
   *injecting jwt service
   * @param jwtService jwt service from nestjs jwt
   */
  constructor(private jwtService: JwtService) {}

  /**
   * generate token method
   * @param data login info
   * @returns jwt
   */
  async generateToken(data: UserLogin) {
    const jwt = await this.jwtService.signAsync({ id: data.id });
    return jwt;
  }

  /**
   * verify token method
   * @param token taking string
   * @returns user data
   */
  async verifyToken(token: string) {
    const data = await this.jwtService.verifyAsync(token);
    if (!data) {
      throw new UnauthorizedException();
    }
    return data;
  }

  /**
   * method for delete token
   * @param response taking response
   * @returns string as the user logout or not
   */
  async deleteToken(@Res({ passthrough: true }) response: Response) {
    response.clearCookie('jwt');
    return {
      message: 'logout',
    };
  }
}
