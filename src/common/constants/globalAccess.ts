/**
 * global access class having all the static variables
 */
export class globalAccess {
  /**
   * declare static variable as role
   */
  static role = '';
}
