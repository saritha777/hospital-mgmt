import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable, tap } from 'rxjs';
import * as cacheManager from 'cache-manager';

/**
 * cache manager interceptor implementing from nest interceptor
 */
@Injectable()
export class CacheManagerInterceptor implements NestInterceptor {
  /**
   * cathing data
   */
  manager = cacheManager.caching({ store: 'memory', max: 100, ttl: 10 });

  /**
   * injecting reflector
   * @param reflector passing reflector
   */
  constructor(private reflector: Reflector) {}

  /**
   * *intercept method
   * @param context taking context
   * @param next next function
   * @returns response
   */
  async intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Promise<Observable<any>> {
    const key = this.reflector.get('cache-key', context.getHandler());

    const cached: any = await this.manager.get(key);

    if (cached) {
      return cached;
    }

    return next.handle().pipe(
      tap((response) => {
        this.manager.set(key, response);
      }),
    );
  }
}
