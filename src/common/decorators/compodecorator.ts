import { applyDecorators, UseGuards } from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { RolesGuard } from '../guards/roles.guard';
import { Roles } from './roles.decorator';

/**
 * compodecorator having roles as paramenter
 * @param roles taking roles
 * @returns meta data related to decorators
 */
export function CompoDecorators(...roles: any) {
  return applyDecorators(Roles(roles), UseGuards(RolesGuard), ApiBearerAuth());
}
