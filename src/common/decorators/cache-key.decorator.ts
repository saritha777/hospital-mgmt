import { SetMetadata } from '@nestjs/common';
/**
 * cache key decorator
 * @param cacheKeyValue taking cache key value
 * @returns metadata
 */
export const CacheKey = (cacheKeyValue) =>
  SetMetadata('cache-key', cacheKeyValue);
