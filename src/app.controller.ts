import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

/**
 * app controller
 */
@Controller()
export class AppController {
  /**
   * injecting app service
   * @param appService injecting app service for all business logics
   */
  constructor(private readonly appService: AppService) {}

  /**
   * get method for hello
   * @returns string from get hello method
   */
  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
}
