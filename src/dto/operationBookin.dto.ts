import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

/**
 * operation booking dto
 */
export class operationBookingDto {
  /**
   * doctor name property
   */
  @ApiProperty()
  @IsString()
  doctorName: string;

  /**
   * from date property
   */
  @ApiProperty()
  @IsString()
  fromDate: string;

  /**
   * to date property
   */
  @ApiProperty()
  @IsString()
  toDate: string;

  /**
   * staff id pproperty
   */
  @ApiProperty()
  @IsString()
  staffId: string;

  /**
   * operation Theater CategoryId property
   */
  @ApiProperty()
  @IsString()
  operationTheaterCategoryId: number;

  /**
   * operation category id property
   */
  @ApiProperty()
  @IsString()
  operationCategoryId: number;

  /**
   * operation Category Name property
   */
  operationCategoryName: string;
}
