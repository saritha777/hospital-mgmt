import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

/**
 * login dto
 */
export class LoginDto {
  /**
   * email property
   */
  @ApiProperty()
  @IsString()
  email: string;

  /**
   * password property
   */
  @ApiProperty()
  @IsString()
  password: string;
}
