import { ApiProperty } from '@nestjs/swagger';
import {
  IsInt,
  IsString,
  Matches,
  MaxLength,
  MinLength,
} from 'class-validator';
import { Maintainance } from 'src/entity/maintainance';

/**
 * staff dto
 */
export class StaffDto extends Maintainance {
  /**
   * first name property of staff
   */
  @ApiProperty()
  @IsString()
  firstName: string;

  /**
   * last name property of staff
   */
  @ApiProperty()
  @IsString()
  lastName: string;

  /**
   * email property
   */
  @ApiProperty()
  @IsString()
  email: string;

  /**
   * staff category id property
   */
  @ApiProperty()
  @IsInt()
  staffCategoryId: number;

  /**
   * dob property
   */
  @ApiProperty()
  @IsString()
  DOB: string;

  /**
   * phone number property
   */
  @ApiProperty()
  @IsString()
  phoneNo: string;

  /**
   * address 1 property
   */
  @ApiProperty()
  @IsString()
  addressLine1: string;

  /**
   * address 2 property
   */
  @ApiProperty()
  @IsString()
  addressLine2: string;

  /**
   * city property
   */
  @ApiProperty()
  @IsString()
  city: string;

  /**
   * state property
   */
  @ApiProperty()
  @IsString()
  state: string;
}
