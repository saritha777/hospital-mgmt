import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

/**
 * staff category dto
 */
export class StaffCategoryDto {
  /**
   *  staffCategoryname property
   */
  @ApiProperty()
  @IsString()
  staffCategoryname: string;
}
