import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

/**
 * Opertaion Theater Category Dto
 */
export class OpertaionTheaterCategoryDto {
  /**
   * operation Theater Name
   */
  @ApiProperty()
  @IsString()
  operationTheaterName: string;
}
