import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

/**
 * operation category dto
 */
export class OperationCategoryDto {
  /**
   * operation Category Name property
   */
  @ApiProperty()
  @IsString()
  operationCategoryName: string;
}
