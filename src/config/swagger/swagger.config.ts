import { SwaggerConfig } from './swagger.interface';

/**
 * swagger config object
 */
export const SWAGGER_CONFIG: SwaggerConfig = {
  /**
   * title property
   */
  title: 'Hospital management',
  /**
   * description property
   */
  description: 'Hospital management',
  /**
   * version property
   */
  version: '1.0',
  /**
   * tags property
   */
  tags: ['templete'],
};
